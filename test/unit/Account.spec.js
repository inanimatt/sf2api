/* global describe, it, expect */

import Vue from 'vue'
import Account from 'src/components/Account'

describe('Account.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      template: '<div><account></account></div>',
      components: { Account }
    }).$mount()
    expect(vm.$el.querySelector('h2').textContent).toBe('Your account')
  })
})

// also see example testing a component with mocks at
// https://github.com/vuejs/vue-loader-example/blob/master/test/unit/a.spec.js#L24-L49
