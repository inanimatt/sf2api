import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import Account from './components/Account.vue'
import App from './components/App.vue'
import EmailVerify from './components/EmailVerify.vue'
import Error404 from './components/Error404.vue'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import PasswordReset from './components/PasswordReset.vue'
import Signup from './components/Signup.vue'

Vue.use(VueResource)
Vue.use(VueRouter)

/* eslint-disable no-new */
export var router = new VueRouter({
  history: true,
  saveScrollPosition: true
})

router.mode = 'html5'

// Set up routing and match routes to components
router.map({
  '/': {
    name: 'home',
    component: Home
  },
  '/index.html': {
    component: Home
  },
  '/account': {
    name: 'account',
    component: Account
  },
  '/login': {
    name: 'login',
    component: Login
  },
  '/password-reset': {
    name: 'password-reset',
    component: PasswordReset
  },
  '/signup': {
    name: 'signup',
    component: Signup
  },
  '/verify': {
    name: 'verify',
    component: EmailVerify
  },
  '/404': {
    component: Error404
  }
})

// Redirect to the home route if any routes are unmatched
router.redirect({
  '*': '/404'
})

// Start the app on the #app div
router.start(App, '#app')

