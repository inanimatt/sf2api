<?php
namespace AppBundle\Transformer;

use AppBundle\Entity;
use League\Fractal;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class User extends Fractal\TransformerAbstract
{
    use ContainerAwareTrait;

    protected $tokenStorage;

    protected $availableIncludes = [
        'subscriptions',
    ];

    protected $defaultIncludes = [];

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function transform($entity)
    {
        if (!$entity instanceof Entity\User) {
            throw new \InvalidArgumentException('Expected a User entity, but got something else.');
        }

        $response = [
            'id' => $entity->getId(),
            'created' => $entity->getCreated()->format('c'),
            'updated' => $entity->getCreated()->format('c'),
            'displayName' => $entity->getDisplayName(),
            'lastLogin' => $entity->getLastLogin() ? $entity->getLastLogin()->format('c') : null,
            'email' => '********',
        ];

        if ($this->tokenStorage->getToken()->getUser() === $entity) {
            $response['email'] = $entity->getUsername();
            $response['emailStatus'] = $entity->getEmailVerified() ? 'verified' : 'unverified';
            $response['subscriberCount'] = count($entity->getSubscriptions());
        }

        return $response;
    }

    public function includeSubscriptions($entity)
    {
        return $this->collection($entity->getSubscriptions(), $this->container->get('transformer_subscription'));
    }
}
