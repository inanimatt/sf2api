<?php
namespace AppBundle\Transformer;

use AppBundle\Entity;
use League\Fractal;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Subscription extends Fractal\TransformerAbstract
{
    use ContainerAwareTrait;

    protected $availableIncludes = [
        'artist',
        'subscriber',
    ];

    protected $defaultIncludes = [];

    public function transform($entity)
    {
        if (!$entity instanceof Entity\Subscription) {
            throw new \InvalidArgumentException('Expected a Subscription entity, but got something else.');
        }

        $response = [
            'id' => $entity->getId(),
            'created' => $entity->getCreated()->format('c'),
            'updated' => $entity->getCreated()->format('c'),
            'artistId' => $entity->getArtist()->getId(),
            'subscriberId' => $entity->getSubscriber()->getId(),
        ];

        return $response;
    }

    public function includeArtist($entity)
    {
        return $this->item($entity->getArtist(), $this->container->get('transformer_user'));
    }

    public function includeSubscriber($entity)
    {
        return $this->item($entity->getSubscriber(), $this->container->get('transformer_user'));
    }
}
