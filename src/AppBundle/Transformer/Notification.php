<?php
namespace AppBundle\Transformer;

use AppBundle\Entity;
use League\Fractal;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Notification extends Fractal\TransformerAbstract
{
    use ContainerAwareTrait;

    protected $availableIncludes = [
        'artist',
    ];

    protected $defaultIncludes = [];

    public function transform($entity)
    {
        if (!$entity instanceof Entity\Notification) {
            throw new \InvalidArgumentException('Expected a Notification entity, but got something else.');
        }

        $response = [
            'id' => $entity->getId(),
            'created' => $entity->getCreated()->format('c'),
            'updated' => $entity->getCreated()->format('c'),
            'artistId' => $entity->getArtist()->getId(),
            'url' => $entity->getUrl(),
            'types' => $entity->getTypes(),
            'isVerified' => $entity->getIsVerified(),
        ];

        return $response;
    }

    public function includeArtist($entity)
    {
        return $this->item($entity->getArtist(), $this->container->get('transformer_user'));
    }
}
