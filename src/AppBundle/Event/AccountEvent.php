<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use AppBundle\Entity;

class AccountEvent extends Event
{
    protected $account;

    public function __construct(Entity\User $account)
    {
        $this->account = $account;
    }

    public function getAccount()
    {
        return $this->account;
    }
}
