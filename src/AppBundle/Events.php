<?php
namespace AppBundle;

final class Events
{
    const ACCOUNT_CREATED = 'app.account_created';
    const EMAIL_CHANGED = 'app.email_changed';
    const PASSWORD_RESET = 'app.password_reset';

    const NOTIFICATION_CREATED = 'app.notification_created';
    const NOTIFICATION_EDITED = 'app.notification_edited';
    const NOTIFICATION_VERIFIED = 'app.notification_verified';
}
