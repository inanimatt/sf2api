<?php

namespace AppBundle\Controller;

use AppBundle\Entity;
use AppBundle\Event;
use AppBundle\Events;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/account")
 */
class AccountController extends Controller
{
    use Behaviour\JsonOutput;

    /**
     * @Route("/", name="account")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $resource = new Item($this->getUser(), $this->get('transformer_user'));

        return $this->generateJson($resource, $request);
    }

    /**
     * @Route("/", name="account_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($this->getUser());
        $em->flush();

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/login", name="login")
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        $token = $this->get('jwt_coder')->encode([
            'userId' => $this->getUser()->getId(),
        ]);

        $userRepo = $this->get('user_repository');
        $userRepo->updateLastLogin($this->getUser());
        $userRepo->save($this->getUser());

        return new JsonResponse([
            'data' => [
                'token' => $token,
            ]
        ]);
    }

    /**
     * @Route("/signup", name="signup")
     * @Method("POST")
     */
    public function signupAction(Request $request)
    {
        $userRepo = $this->get('user_repository');
        if (!$userRepo->canCreateNewAccounts()) {
            return $this->generateErrorResponse('Sorry, we aren\'t taking new accounts right now.');
        }


        $user = new Entity\User;
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            $messages = (string) $form->getErrors(true);

            return $this->generateErrorResponse($messages);
        }

        $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getPassword()));

        $userRepo->save($user);

        $this->get('event_dispatcher')->dispatch(Events::ACCOUNT_CREATED, new Event\AccountEvent($user));

        $token = $this->get('jwt_coder')->encode([
            'userId' => $user->getId(),
        ]);

        return new JsonResponse([
            'data' => [
                'status' => 'success',
                'token' => $token,
            ],
        ]);
    }

    /**
     * @Route("/", name="edit")
     * @Method("PATCH")
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        $previousPassword = $user->getPassword();
        $previousEmail = $user->getUsername();

        $form = $this->createForm('AppBundle\Form\UserType', $user, [
            'method' => 'PATCH',
        ]);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            $messages = (string) $form->getErrors(true);
            return $this->generateErrorResponse($messages);
        }

        if ($user->getPassword() !== $previousPassword) {
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getPassword()));
        }

        if ($user->getEmail() !== $previousEmail) {
            $this->get('event_dispatcher')->dispatch(Events::EMAIL_CHANGED, new Event\AccountEvent($user));
        }

        $userRepo = $this->get('user_repository');
        $userRepo->save($user);

        $resource = new Item($this->getUser(), $this->get('transformer_user'));

        return $this->generateJson($resource, $request);
    }

    /**
     * @Route("/verify", name="verify")
     * @Method("POST")
     */
    public function verifyAction(Request $request)
    {
        $token = $request->request->get('token', '');

        $user = $this->getUser();

        if (!hash_equals($user->getEmailToken(), $token) || ($user->getEmailTokenExpires() < date_create())) {
            return $this->generateErrorResponse('Invalid token');
        }

        // mark as verified and remove token & expiry
        $user->setEmailToken(null);
        $user->setEmailTokenExpires(null);
        $user->setEmailVerified(true);

        $this->get('user_repository')->save($user);

        return new JsonResponse([
            'data' => [
                'status' => 'success',
                'message' => 'Email address verified',
            ],
        ]);
    }

    /**
     * @Route("/password-reset", name="password_reset_start")
     * @Method("POST")
     */
    public function beginResetAction(Request $request)
    {
        $email = $request->request->get('email');

        $userRepo = $this->get('user_repository');
        $user = $userRepo->findOneByUsername($email);

        if (!$user) {
            return $this->generateErrorResponse('No such account');
        }

        $this->get('event_dispatcher')->dispatch(Events::PASSWORD_RESET, new Event\AccountEvent($user));

        return new JsonResponse([
            'data' => [
                'status' => 'success',
                'message' => 'Check your email for a reset token',
            ],
        ]);
    }

    /**
     * @Route("/password-reset/reset", name="password_reset")
     * @Method("POST")
     */
    public function resetAction(Request $request)
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $reset = $request->request->get('token');

        $userRepo = $this->get('user_repository');
        $user = $userRepo->findOneByUsername($email);

        if (!$user || !$password || !$user->getResetToken() || !hash_equals($user->getResetToken(), $reset) || ($user->getResetTokenExpires() < date_create())) {
            return $this->generateErrorResponse('Invalid details');
        }

        $user->setResetToken(null);
        $user->setResetTokenExpires(null);
        $user->setPassword($password);

        $violations = $this->get('validator')->validate($user);
        if ($violations->count() > 0) {
            return $this->generateErrorResponse('New password is invalid');
        }

        $user->setPassword($this->get('security.password_encoder')->encodePassword($user, $user->getPassword()));
        $userRepo->save($user);

        $jwt = $this->get('jwt_coder')->encode([
            'userId' => $user->getId(),
        ]);

        return new JsonResponse([
            'data' => [
                'status' => 'success',
                'message' => 'New password accepted',
                'token' => $jwt,
            ],
        ]);
    }
}
