<?php

namespace AppBundle\Controller;

use AppBundle\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DefaultController extends Controller
{
    use Behaviour\JsonOutput;

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return new JsonResponse([
            'status' => 'Hello ' . $this->getUser()->getUsername(),
            'roles' => $this->getUser()->getRoles(),
        ]);
    }

    public function bootstrapAction(Request $request)
    {
        return new BinaryFileResponse(
            $this->getParameter('kernel.root_dir').'/../web/index.html',
            BinaryFileResponse::HTTP_OK, // TODO: be smarter here and return 404 on invalid URLs
            [], // headers
            true, // public
            null, // content disposition
            true, // auto e-tag
            true // auto last-modified
        );
    }
}
