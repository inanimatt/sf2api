<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity("username", message="user.username.duplicate")
 */


class User implements UserInterface
{
    use Behaviour\Timestamp;

    /**
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=128, nullable=false, unique=true)
     * @Assert\Email(message="user.email")
     * @Assert\NotBlank(message="user.username.blank")
     * @Assert\Length(min=8, max=128, minMessage="user.username.too_short", maxMessage="user.username.too_long")
     */
    private $username = 'invalid';

    /**
     * @ORM\Column(name="display_name", type="string", length=64, nullable=true)
     * @Assert\Length(max=64, maxMessage="user.display_name.too_long")
     */
    private $displayName = 'anon.';

    /**
     * @ORM\Column(name="password", type="string", length=128, nullable=false)
     * @Assert\NotBlank(message="user.password.blank")
     * @Assert\Length(min=8, max=128, minMessage="user.password.too_short", maxMessage="user.password.too_long")
     */
    private $password;

    /**
     * @ORM\Column(name="email_verified", type="boolean", nullable=false)
     */
    private $emailVerified = 0;

    /**
     * @ORM\Column(name="email_token", type="string", length=42, nullable=true)
     */
    private $emailToken;

    /**
     * @ORM\Column(type="datetime", name="email_token_expires", nullable=true)
     */
    private $emailTokenExpires;

    /**
     * @ORM\Column(name="reset_token", type="string", length=42, nullable=true)
     */
    private $resetToken;

    /**
     * @ORM\Column(type="datetime", name="reset_token_expires", nullable=true)
     */
    private $resetTokenExpires;


    /**
     * @ORM\Column(type="datetime", name="last_login", nullable=true)
     */
    private $lastLogin;

    function __construct() {
        $this->setCreated(date_create());
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setUsername(string $username)
    {
        $username = mb_strtolower($username);

        if ($this->username !== $username) {
            $this->setEmailVerified(false);
        }

        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setEmail(string $email)
    {
        $this->setUsername($email);
    }
    
    public function getEmail(): string
    {
        return $this->getUsername();
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        return;
    }

    public function setLastLogin(\DateTime $lastLogin = null)
    {
        $this->lastLogin = $lastLogin ? clone $lastLogin : null;
    }

    public function getLastLogin()
    {
        return $this->lastLogin ? clone $this->lastLogin : null;
    }

    public function setDisplayName(string $displayName)
    {
        $this->displayName = $displayName;
    }

    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function setEmailVerified(bool $emailVerified)
    {
        $this->emailVerified = $emailVerified;
    }

    public function getEmailVerified(): bool
    {
        return $this->emailVerified;
    }

    public function setEmailToken(string $emailToken = null)
    {
        $this->emailToken = $emailToken;
    }

    public function getEmailToken(): string
    {
        return $this->emailToken;
    }

    public function setEmailTokenExpires(\DateTime $emailTokenExpires = null)
    {
      $this->emailTokenExpires = $emailTokenExpires ? clone $emailTokenExpires : null;
    }
    
    public function getEmailTokenExpires()
    {
        return $this->emailTokenExpires ? clone $this->emailTokenExpires : null;
    }

    public function setResetToken(string $resetToken)
    {
        $this->resetToken = $resetToken;
    }

    public function getResetToken(): string
    {
        return $this->resetToken;
    }

    public function setResetTokenExpires(\DateTime $resetTokenExpires = null)
    {
      $this->resetTokenExpires = $resetTokenExpires ? clone $resetTokenExpires : null;
    }
    
    public function getResetTokenExpires()
    {
        return $this->resetTokenExpires ? clone $this->resetTokenExpires : null;
    }
}
