<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    // NOTE: This loads by ID if not email address
    public function loadUserByUsername($username)
    {
        return strpos($username, '@') ? $this->findOneByUsername($username) : $this->find($username);
    }

    public function create($username)
    {
        $entity = new User();
        $entity->setUsername($username);

        return $entity;
    }

    public function save(User $entity)
    {
        $entity->setUpdated(date_create());

        $entityManager = $this->getEntityManager();
        $entityManager->persist($entity);
        $entityManager->flush();

        return $entity;
    }

    public function updateLastLogin(User $entity)
    {
        $entity->setLastLogin(date_create());

        return $entity;
    }

    public function canCreateNewAccounts()
    {
        return true;

        // Alternatively limit accounts by number of existing accounts… or whatever
        // return $this->getEntityManager()->getConnection()->fetchColumn('SELECT COUNT(*) FROM user', [], 0) < 500;
    }
}
