<?php
namespace AppBundle\EventListener;

use AppBundle\Event;
use AppBundle\Events;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Doctrine\DBAL\Driver\Connection;

class Notification
{
    protected $dbal;
    protected $mailer;
    protected $notificationRepository;
    protected $router;
    protected $twig;

    // router, user_repository, mailer, twig
    public function __construct(RouterInterface $router, ObjectRepository $notificationRepository, \Swift_Mailer $mailer, \Twig_Environment $twig, Connection $dbal)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->dbal = $dbal;
        $this->notificationRepository = $notificationRepository;
    }

    public function sendPreview(Event\NotificationEvent $event)
    {
        $notification = $event->getNotification();

        // Generate token
        $token = rtrim(strtr(base64_encode(random_bytes(16)), '+/', '-_'), '=');
        $notification->setEmailToken($token);

        // Save notification
        $this->notificationRepository->save($notification);

        // Generate URL
        $verificationUrl = vsprintf('%s://%s/%sverifyNotification?id=%s&token=%s', [
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $this->router->getContext()->getBaseUrl(),
            $notification->getId(),
            $token,
        ]);

        $editUrl = vsprintf('%s://%s/%seditNotification?id=%s&token=%s', [
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $this->router->getContext()->getBaseUrl(),
            $notification->getId(),
            $token,
        ]);

        $cancelUrl = vsprintf('%s://%s/%scancelNotification?id=%s&token=%s', [
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $this->router->getContext()->getBaseUrl(),
            $notification->getId(),
            $token,
        ]);

        // Create & send mail
        $params = [
            'verify_url' => $verificationUrl,
            'edit_url' => $editUrl,
            'cancel_url' => $cancelUrl,
            'artist' => $notification->getArtist(),
            'notification' => $notification,
        ];

        $message = \Swift_Message::newInstance()
            ->setSubject('[Hey I\'m Open] Verify your notification')
            ->setFrom('soli@heyimopen.com')
            ->setTo($notification->getArtist()->getUsername())
            ->setBody($this->twig->render('email/notificationPreview.txt.twig', $params))
            ->addPart($this->twig->render('email/notificationPreview.html.twig', $params), 'text/html')
        ;

        $headers = $message->getHeaders();
        $headers->addTextHeader('o:tag', 'preview');

        $this->mailer->send($message);
    }

    public function enqueueMails(Event\NotificationEvent $event)
    {
        $notification = $event->getNotification();

        $messageData = [
            'notification' => $notification->getId(),
        ];

        $this->dbal->executeQuery(sprintf('INSERT INTO mail (subscriber_id, notification_id, status, created_at, updated_at)
            SELECT subscriber_id, "%s", "unsent", now(), now()
            FROM subscription s
            JOIN user u on s.subscriber_id = u.id
            WHERE s.artist_id = :artist
            AND u.email_verified = 1', $notification->getId()), [
                'artist' => $notification->getArtist()->getId(),
        ]);
    }
}
