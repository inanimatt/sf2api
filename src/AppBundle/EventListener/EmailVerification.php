<?php
namespace AppBundle\EventListener;

use AppBundle\Event;
use AppBundle\Events;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class EmailVerification
{
    protected $mailer;
    protected $router;
    protected $twig;
    protected $userRepository;

    // router, user_repository, mailer, twig
    public function __construct(RouterInterface $router, ObjectRepository $userRepository, \Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->userRepository = $userRepository;
    }

    public function onEmailChanged(Event\AccountEvent $event)
    {
        $account = $event->getAccount();

        $account->setEmailVerified(false);

        // Generate token
        $token = rtrim(strtr(base64_encode(random_bytes(16)), '+/', '-_'), '=');
        $account->setEmailToken($token);

        // Set token expiry
        $expires = date_create('+2 days');
        $account->setEmailTokenExpires($expires);

        // Save user
        $this->userRepository->save($account);

        // Generate URL
        $verificationUrl = vsprintf('%s://%s/%sverify?token=%s', [
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $this->router->getContext()->getBaseUrl(),
            $token,
        ]);

        // Create & send mail
        $message = \Swift_Message::newInstance()
            ->setSubject('[Hey I\'m Open] Verify your email address')
            ->setFrom('soli@heyimopen.com')
            ->setTo($account->getUsername())
            ->setBody($this->twig->render('email/verify.txt.twig', [
                'url' => $verificationUrl,
                'account' => $account,
            ]))
            ->addPart($this->twig->render('email/verify.html.twig', [
                'url' => $verificationUrl,
                'account' => $account,
            ]), 'text/html')
        ;

        $headers = $message->getHeaders();
        $headers->addTextHeader('o:tag', 'email-verify');

        $this->mailer->send($message);
    }

    public function onPasswordReset(Event\AccountEvent $event)
    {
        $account = $event->getAccount();

        // Generate token
        $token = rtrim(strtr(base64_encode(random_bytes(16)), '+/', '-_'), '=');
        $account->setResetToken($token);

        // Set token expiry
        $expires = date_create('+1 day');
        $account->setResetTokenExpires($expires);

        // Save user
        $this->userRepository->save($account);

        // Generate URL
        $verificationUrl = vsprintf('%s://%s/%spassword-reset?token=%s', [
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $this->router->getContext()->getBaseUrl(),
            $token,
        ]);

        // Create & send mail
        $message = \Swift_Message::newInstance()
            ->setSubject('[Hey I\'m Open] Reset your password')
            ->setFrom('soli@heyimopen.com')
            ->setTo($account->getUsername())
            ->setBody($this->twig->render('email/password-reset.txt.twig', [
                'url' => $verificationUrl,
                'account' => $account,
            ]))
            ->addPart($this->twig->render('email/password-reset.html.twig', [
                'url' => $verificationUrl,
                'account' => $account,
            ]), 'text/html')
        ;

        $headers = $message->getHeaders();
        $headers->addTextHeader('o:tag', 'password-reset');

        $this->mailer->send($message);
    }
}
