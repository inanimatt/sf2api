<?php
namespace AppBundle\Http;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

// Braindead CORS header kernel - just allow everything
class CorsHeaderKernel implements HttpKernelInterface
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        if (!$request->isMethod('OPTIONS')) {
            return $this->app->handle($request, $type, $catch);
        }

        $response = new Response();
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, OPTIONS, DELETE');
        $response->headers->set('Access-Control-Max-Age', 3600);
        $response->headers->set('Access-Control-Allow-Headers', 'Authorization, Accept-Encoding, Content-Type, Cache-Control, X-Requested-With');

        return $response;
    }

    public function terminate(Request $request, Response $response)
    {
        $this->app->terminate($request, $response);
    }
}
