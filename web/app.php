<?php
umask(0001);

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../var/bootstrap.php.cache';

// Use APC for autoloading to improve performance (if available)
// Change 'api' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.
if (function_exists('apc_store')) {
    $apcLoader = new ApcClassLoader('api', $loader);
    $loader->unregister();
    $apcLoader->register(true);
}

require_once __DIR__.'/../app/AppKernel.php';

if (getenv('APP_ENV') === 'dev') {
    Debug::enable();
    $kernel = new AppKernel('dev', true);
    $kernel->loadClassCache();
} else {
    require_once __DIR__.'/../app/AppCache.php';
    $kernel = new AppKernel('prod', false);
    $kernel->loadClassCache();
    $kernel = new AppCache($kernel);
}

$kernel = new AppBundle\Http\CorsHeaderKernel($kernel);
$kernel = new AppBundle\Http\JsonRequestKernel($kernel);


// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
